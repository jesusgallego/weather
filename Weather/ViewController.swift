//
//  ViewController.swift
//  Weather
//
//  Created by Master Móviles on 4/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    
    let OW_URL_BASE = "http://api.openweathermap.org/data/2.5/weather?lang=es&units=metric&appid=1adb13e22f23c3de1ca37f3be90763a9&q="
    let OW_URL_BASE_ICON = "http://openweathermap.org/img/w/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        cityTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onSearchButtonClick(_ sender: AnyObject) {
        consultarTiempo(localidad:cityTextField.text!)
    }
    
    
    
    func consultarTiempo(localidad:String) {
        let urlString = OW_URL_BASE+localidad
        let url = URL(string:urlString)
        let dataTask = URLSession.shared.dataTask(with: url!) {
            datos, respuesta, error in
            do {
                let jsonStd = try JSONSerialization.jsonObject(with: datos!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
                let weather = jsonStd["weather"]! as! [AnyObject]
                let currentWeather = weather[0] as! [String:AnyObject]
                let descripcion = currentWeather["description"]! as! String
                print("El tiempo en \(localidad) es: \(descripcion)")
                //Estamos bajándonos la imagen pero todavía no la usamos
                let icono = currentWeather["icon"]! as! String
                if let urlIcono = URL(string: self.OW_URL_BASE_ICON+icono+".png" ) {
                    if let datosIcono = try? Data(contentsOf: urlIcono) {
                        let imagenIcono = UIImage(data: datosIcono)
                        OperationQueue.main.addOperation {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.iconImage.image = imagenIcono
                            self.stateLabel.text = descripcion
                        }
                    }
                }
            } catch {
                print("ERROR")
            }
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        dataTask.resume()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let number = Int(string)
        return (number == nil)
    }
    
}

